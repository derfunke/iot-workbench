# ioT "hello world" for Wemos D1

This is a simple IoT **Hello World** kind of program. It was written for a **Wemos D1 Pro** and all it does it connect to [shiftr.io](https://shiftr.io) and publish a **hello world** message every second. This can be a good ground to start your own IoT project.

You will need:
- to create your own [shiftr.io](https://shiftr.io) account
- to have created a **namespace** within [shiftr.io](https://shiftr.io) for your IoT project (I called mine `heartbeat` but it can be anything)
- to have created a token in [shiftr.io](https://shiftr.io) to **authenticate** your device

You will need to open the `config.h` and change the settings there for your own settings.

#### Ideas

Try out a platform that supports rules and historical visualization like [thingsboard.io](https://thingsboard.io).