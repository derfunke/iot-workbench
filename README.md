## IoT workbench

This is a few starter-code projects for IoT related projects to be usied in the classroom. 
Running on the ESP8266 chipset, using various protocols and two mqtt broker platforms.

There's a [**getting started** exercise here](arduino/mqtt_sensor_publish/docs/session.md)

